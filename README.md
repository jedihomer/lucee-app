# Example Lucee App

Example Lucee app based in the image jedihomer/lucee

Ensure you have docker and docker-compose installed then clone this project and run `docker-compose up -d` and it should build and run the app

To build, the following variables need to be setup in Gitlab

CI_ACCESS_TOKEN
CI_REGISTRY
CI_REGISTRY_PASSWORD
CI_REGISTRY_USER
CI_LUCEE_PASSWORD
