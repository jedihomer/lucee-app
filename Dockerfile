FROM jedihomer/lucee:5.3.2.77
LABEL maintainer="jedi@jedihomer.net"

# set values for lucee configuration
ARG VERSION
ARG LUCEE_PASSWORD

# Set the password so the server can be set up
ENV LUCEE_PASSWORD $LUCEE_PASSWORD

# Add the code to the image
ADD code /app

# Version the software
RUN sed -i "s/{app_version}/$VERSION/" /app/webroot/healthcheck/Application.cfc
RUN sed -i "s/{app_version}/$VERSION/" /app/webroot/healthcheck/build/Application.cfc

# Fix the rights so we can run as a non-root user
RUN chown -R www-data:www-data $CATALINA_HOME $LUCEE_HOME /app/webroot

# Update the versions file to show this build
RUN echo "App Image Built: $(date)" >> /app/versions.txt; echo "App Image Version: $VERSION" >> /app/versions.txt

# Set the user, BEFORE we run the healthcheck which compiles the mappings!
USER www-data

# Spin up lucee and run the healthcheck
RUN $CATALINA_HOME/bin/healthcheck.sh

# Reset the rights
USER root
RUN chown -R root /app/webroot

# set user and expose port
USER www-data
EXPOSE 8888
